# Minecraft Base Project

L'ojectif de ce projet est de mettre en place plusieurs organes d'analyses et de contrôle afin de superviser la production et la consommation d'énergie simulée par un monde virtuel sur Minecraft. Nous utilisons donc les technologies de l'information et de la communication.

## Getting Started

### Prérequis

Il faut installer Java JDK avec la version 8 :

```
https://www.oracle.com/java/technologies/javase/javase-jdk8-downloads.html
```

Il faut aussi installer le minecraft modé spécial Electrical Age :

```
https://github.com/patrudaz/ElectricalAge/archive/refs/heads/develop.zip
```

Le monde Minecraft est au lien suivant :
```
https://cyberlearn.hes-so.ch/pluginfile.php/3567904/mod_label/intro/SIn_2021_0.1.0.zip
```

Pour utiliser le client web, il faut télécharger un fichier à l'adresse suivante :
```
https://cyberlearn.hes-so.ch/pluginfile.php/3587046/mod_label/intro/WebClient.zip
```

### Installation

En ce qui concerne l'installation, veuillez vous référer au document suivant **EA-ToolsToInstall.pdf** se trouvant à la source du projet.

## Lancement

1) Lancer l'invite de commande
2) Se rendre à l'emplacement du fichier **Minecraft.jar** comme ci-dessous :
```
cd ...\hei-synd-221-sin-minecraft-baseproject\out\artifacts\Minecraft_jar
```
3) Exécuter le fichier **Minecraft.jar** exactement comme ci-dessous :
```
java -jar Minecraft.jar https://influx.sdi-hevs.ch/ux.sdi-hevs.ch/ SIn32 localhost 1502
```

4) Pour un aperçu de la base de donnée, ce lien vous enverra sur le serveur **Grafana** :
```
https://grafana.sdi.hevs.ch/
```
Il vous faudra le nom d'utilisateur et le mot de passe pour se connecter :
```
User : SIn32
Password : 2604d6be09f8f223512325d322e90c4a
```

5) Pour accéder au client web, il faut inscrire le chemin du fichier **index.html** dans la barre de votre navigateur web.

## Built With

* [Maven](https://maven.apache.org/) - Dependency Management

## Authors

* **Clivaz Xavier** - *Student infotronic* - [Kevax24](https://gitlab.com/Kevax24)
* **Grobéty Christophe** - *Student infotronic* - [christophe_grobety](https://gitlab.com/christophe_grobety)
* **Jozipovic Thien** - *Student infotronic* - [TJ51](https://gitlab.com/TJ51)

## Acknowledgments

Nous remercions Mr. [Dominique Gabioud](https://gitlab.com/DominiqueGabioud) et Mr. [Patrice Rudaz](https://gitlab.com/patrudaz) pour leur aide apportée durant ce projet.

