package ch.hevs.isi;

import ch.hevs.isi.core.BinaryDataPoint;
import ch.hevs.isi.core.DataPoint;
import ch.hevs.isi.core.FloatDataPoint;

import java.io.IOException;
import java.util.Timer;
import java.util.TimerTask;

/**
 * The SmartControl handles the regulation of the system and hence control the grid elements
 */
public class SmartControl {
    //Private attribute
    private static SmartControl instance = null;

    //Output
    BinaryDataPoint solarSW;
    BinaryDataPoint windSW;
    FloatDataPoint coalSP;
    FloatDataPoint factorySP;

    //Input
    FloatDataPoint battery;
    FloatDataPoint coalAmount;
    FloatDataPoint battUsage;

    /**
     * Constructor
     */
    private SmartControl(){
        solarSW = (BinaryDataPoint) DataPoint.getDataPointFromLabel("REMOTE_SOLAR_SW");
        windSW = (BinaryDataPoint) DataPoint.getDataPointFromLabel("REMOTE_WIND_SW");
        coalSP = (FloatDataPoint) DataPoint.getDataPointFromLabel("REMOTE_COAL_SP");
        factorySP = (FloatDataPoint) DataPoint.getDataPointFromLabel("REMOTE_FACTORY_SP");
        battery = (FloatDataPoint) DataPoint.getDataPointFromLabel("BATT_CHRG_FLOAT");
        coalAmount = (FloatDataPoint) DataPoint.getDataPointFromLabel("COAL_AMOUNT");
        battUsage = (FloatDataPoint) DataPoint.getDataPointFromLabel("BATT_P_FLOAT");
    }

    /**
     * The static method getInstance () returns a reference to the singleton.
     * It creates the single SmartControl object if it does not exist.
     * @return a reference to the singleton
     */
    public static SmartControl getInstance(){
        if(instance == null){
            instance = new SmartControl();
        }
        return instance;
    }

    /**
     * In charge of regulating the minecraft world. Controls Factory and coal plant setpoints depending on
     * the battery. Score obtained: 403K
     */
    public void doControl(){
        try {
            float powerUsage = battUsage.getValue()*6000 - 3000;    //negative when consumption > production

            windSW.setValue(true);
            solarSW.setValue(true);

            //Control wind, solar, and factory depending on battery charge
            if(battery.getValue() >= 0.8f){
                windSW.setValue(false);
                solarSW.setValue(false);
                factorySP.setValue(1);
            }else if(battery.getValue() <= 0.4f){
                factorySP.setValue(0);
            }else
                factorySP.setValue(1);

            //Control coal plant depending on battery charge/consumption
            if(coalAmount.getValue() != 0) {
                if (powerUsage < 250) {
                    coalSP.setValue(0.5f);              //Need more energy => 250W from coal plant
                } else if (powerUsage > 250)
                    coalSP.setValue(0);                 //Sufficient energy provided from wind/solar

                if (battery.getValue() >= 0.7f)
                    coalSP.setValue(0);                 //Battery charged at +70% => coal set to zero
                else if (battery.getValue() <= 0.5f)
                    coalSP.setValue(1);                 //Battery dropping below 50% => coal supply required
            }else
                coalSP.setValue(0);                     //Out of coal
        } catch (IOException e){
            e.printStackTrace();
        }
    }

    /**
     * Sets a control routine every 5 seconds
     */
    public void timedControl(){
        Timer pollTimer = new Timer();

        pollTimer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                try {
                    doControl();
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        },0, 5000);
    }
}
