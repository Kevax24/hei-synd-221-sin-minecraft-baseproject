package ch.hevs.isi.core;

import java.io.IOException;

/**
 * Datapoint holding a boolean type value
 */
public class BinaryDataPoint extends DataPoint{
    //Private attribute
    private boolean value;

    /**
     * Constructor
     * @param label label of the field
     * @param isOutput input if true
     */
    public BinaryDataPoint(String label, boolean isOutput) {
        super(label, isOutput);
    }

    /**
     * Modifies the datapoint's value
     * @param value boolean value
     * @throws IOException
     */
    public void setValue(boolean value) throws IOException {
        this.value = value;                     //Changes value
        dbc.onNewValue(this);             //Notifies connectors of the value change
        wc.onNewValue(this);
        fc.onNewValue(this);
    }

    /**
     * Reads the datapoint's value
     * @return Returns the value (boolean)
     */
    public boolean getValue(){
        return value;
    }
}
