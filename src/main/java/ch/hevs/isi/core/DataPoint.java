package ch.hevs.isi.core;

import ch.hevs.isi.database.DatabaseConnector;
import ch.hevs.isi.field.FieldConnector;
import ch.hevs.isi.web.WebConnector;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * Datapoint is the digital twin holding the label and the value of the field
 */
public class DataPoint {
    //Private attributes
    private static Map<String, DataPoint> dataPointMap = new HashMap<>();
    private String label;
    private boolean isOutput;

    //Connectors - Singletons
    DatabaseConnector dbc = DatabaseConnector.getInstance();
    WebConnector wc = WebConnector.getInstance();
    FieldConnector fc = FieldConnector.getInstance();

    /**
     * Constructor
     * @param label label of the field
     * @param isOutput input if true
     */
    protected DataPoint(String label, boolean isOutput){
        this.label = label;
        this.isOutput = isOutput;

        dataPointMap.put(label, this);      //Add new member to the map
    }

    /**
     * Modifies the datapoint's value
     * @param value boolean value
     * @throws IOException
     */
    public void setValue(boolean value) throws IOException {}

    /**
     * Modifies the datapoint's value
     * @param value float value
     * @throws IOException
     */
    public void setValue(float value) throws IOException {}

    /**
     * Will get a datapoint, corresponding to the given label, from the map
     * (if it exists)
     * @param label String label eg: WIND_P_FLOAT
     * @return Returns a datapoint object from the map
     */
    public static DataPoint getDataPointFromLabel(String label){
        return dataPointMap.get(label);
    }

    /**
     * Reads the datapoint's label
     * @return Returns the label (string)
     */
    public String getLabel(){
        return label;
    }

    /**
     * Main function used to test core component
     * @param args
     * @throws IOException
     */
    public static void main(String[] args) throws IOException {
        //Creates datapoints
        BinaryDataPoint dp1 = new BinaryDataPoint("REMOTE_WIND_SW", true);
        FloatDataPoint dp2 = new FloatDataPoint("SOLAR_P_FLOAT", true);

        //Add them in the map
        dataPointMap.put("REMOTE_WIND_SW", dp1);
        dataPointMap.put("SOLAR_P_FLOAT", dp2);

        //Setting new values
        dataPointMap.get("REMOTE_WIND_SW").setValue(false);
        dataPointMap.get("SOLAR_P_FLOAT").setValue(200);
        dataPointMap.get("REMOTE_WIND_SW").setValue(true);
        dataPointMap.get("SOLAR_P_FLOAT").setValue(420);
    }
}
