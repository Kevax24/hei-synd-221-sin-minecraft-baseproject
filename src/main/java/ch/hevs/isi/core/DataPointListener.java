package ch.hevs.isi.core;

import java.io.IOException;

/**
 * Interface for onNewValue() called whenever a value is modified
 */
public interface DataPointListener {
    void onNewValue(FloatDataPoint fdp) throws IOException;
    void onNewValue(BinaryDataPoint bdp) throws IOException;
}
