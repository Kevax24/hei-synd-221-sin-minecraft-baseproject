package ch.hevs.isi.core;

import java.io.IOException;

/**
 * Datapoint holding a float type value
 */
public class FloatDataPoint extends DataPoint{
    //Private attribute
    private float value;

    /**
     * Constructor
     * @param label label of the field
     * @param isOutput input if true
     */
    public FloatDataPoint(String label, boolean isOutput) {
        super(label, isOutput);
    }

    /**
     * Modifies the datapoint's value
     * @param value float value
     * @throws IOException
     */
    public void setValue(float value) throws IOException {
        this.value = value;                 //Changes value
        dbc.onNewValue(this);          //Notifies connectors of the value change
        wc.onNewValue(this);
        fc.onNewValue(this);
    }

    /**
     * Reads the datapoint's value
     * @return Returns the value (float)
     */
    public float getValue(){
        return value;
    }
}
