package ch.hevs.isi.database;

import ch.hevs.isi.core.BinaryDataPoint;
import ch.hevs.isi.core.DataPointListener;
import ch.hevs.isi.core.FloatDataPoint;
import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Base64;

/**
 * DatabaseConnector establishes a connection to the influxDB
 * database and writes data to it.
 */
public class DatabaseConnector implements DataPointListener {

    private static DatabaseConnector instance = null;

    private DatabaseConnector(){
    }

    /**
     * Creates the single DatabaseConnector object if it does not exist.
     * @return a reference to the singleton
     */
    public static DatabaseConnector getInstance(){
        if(instance == null){
            instance = new DatabaseConnector();
        }
        return instance;
    }

    /**
     * Push to database label and value
     * @param label DataPoint's label (String)
     * @param value DataPoint's label (String)
     * @throws IOException
     */
    public void pushToDatabase(String label, String value) throws IOException{
        URL url = new URL("https://influx.sdi.hevs.ch/write?db=SIn32");        //Create an url object with the server's url
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();    //Instantiate a HttpURLConnection and give its reference

        String userpass = "SIn32:2604d6be09f8f223512325d322e90c4a";                 //User and password to access the database
        String encoding = Base64.getEncoder().encodeToString(userpass.getBytes());

        connection.setRequestProperty("Authorization", "Basic " + encoding);        //Authenticate header in the request
        connection.setRequestProperty("Content-Type", "binary/octet-stream");
        connection.setRequestMethod("POST");
        connection.setDoOutput(true);                                               //True => contains a message body

        OutputStreamWriter writer = new OutputStreamWriter(connection.getOutputStream());
        writer.write("SInWorld " + label + "=" + value);                        //Write only body of the http request
        writer.flush();

        BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));

        int responseCode = connection.getResponseCode();                            //Get response's status

        if(responseCode == 204){                                                    //204 => writing operation was a success
            while ((in.readLine()) != null){}                                       //Read all data
            System.out.println("Response code:" + responseCode);
        }

        connection.disconnect();                                                    //Close communication
    }

    /**
     * Updates a new float value
     * @param fdp FloatDataPoint (Object)
     */
    @Override
    public void onNewValue(FloatDataPoint fdp){
        try {
            pushToDatabase(fdp.getLabel(), Float.toString(fdp.getValue()));
        }catch (IOException e){
            e.printStackTrace();
            System.out.println("Error on new value FloatDataPoint in DatabaseConnector");
        }
    }

    /**
     * Updates a new binary value
     * @param bdp BinaryDataPoint (Object)
     */
    @Override
    public void onNewValue(BinaryDataPoint bdp) {
        try {
            pushToDatabase(bdp.getLabel(), Boolean.toString(bdp.getValue()));
        }catch (IOException e){
            e.printStackTrace();
            System.out.println("Error on new value BinaryDataPoint in DatabaseConnector");
        }
    }

    /**
     * Main function used to test database component
     * @param args
     */
    public static void main(String[] args) {
        DatabaseConnector db = DatabaseConnector.getInstance();

        try {
            db.pushToDatabase("HOME_P_FLOAT", "584");
            db.pushToDatabase("BATT_CHRG_FLOAT", "0.4");

        }catch (IOException e){
            e.printStackTrace();
        }
    }
}
