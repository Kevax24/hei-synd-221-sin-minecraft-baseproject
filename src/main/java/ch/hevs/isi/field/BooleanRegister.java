package ch.hevs.isi.field;

import ch.hevs.isi.core.BinaryDataPoint;
import ch.hevs.isi.core.DataPoint;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * Register that binds a binaryDatapoint to ModbusAccessor
 */
public class BooleanRegister {
    //Modbus credentials
    static String SERVER_NAME = "localhost";
    static int SERVER_PORT = 1502;

    //Field variables
    int range;
    int offset;
    private int address;
    BinaryDataPoint dataPoint;

    ModbusAccessor ma = new ModbusAccessor(SERVER_PORT, SERVER_NAME);

    //Map containing all booleanRegisters
    static Map<DataPoint, BooleanRegister> bRegisterMap = new HashMap<>();

    /**
     * Constructor
     * @param label Label of the field
     * @param address Address of the the register
     * @param range Range of the value
     * @param offset Offset if it has one
     * @throws IOException
     */
    public BooleanRegister(String label, int address,int range, int offset) throws IOException {
        this.address = address;
        this.range = range;
        this.offset = offset;

        dataPoint = (BinaryDataPoint) BinaryDataPoint.getDataPointFromLabel(label); //Seek the corresponding datapoint
                                                                                    //in the map (core)
        if(dataPoint == null){                                                      //Datapoint not created yet
            if(address == 401 || address == 405) {                                  //It is an input
                dataPoint = new BinaryDataPoint(label, true);
            }else{                                                                  //It is an output
                dataPoint = new BinaryDataPoint(label, false);
            }
        }
        bRegisterMap.put(dataPoint, this);                                          //Stocks the datapoint in the map
    }

    /**
     * Reads the  measure through modbus and update value
     * @throws Exception
     */
    public void read() throws Exception {
        dataPoint.setValue(ma.readBoolean(address));
    }

    /**
     * Writes a new value in minecraft through modbus
     * @throws IOException
     */
    public void write() throws IOException {
        ma.writeBoolean(address, dataPoint.getValue());
    }

    /**
     * Retrieves a register stocked in the map from a datapoint
     * @param bdp BinaryDataPoint
     * @return Returns the corresponding register
     */
    public static BooleanRegister getRegisterFromDataPoint(BinaryDataPoint bdp)
    {
        return bRegisterMap.get(bdp);
    }

    /**
     * Read action for every member of the map. Global update of every register
     * @throws Exception
     */
    public static void poll() throws Exception {
        for(Map.Entry<DataPoint, BooleanRegister> entry : bRegisterMap.entrySet()){
            bRegisterMap.get(entry.getKey()).read();
        }
    }
}
