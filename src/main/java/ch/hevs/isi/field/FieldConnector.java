package ch.hevs.isi.field;

import ch.hevs.isi.core.BinaryDataPoint;
import ch.hevs.isi.core.DataPoint;
import ch.hevs.isi.core.DataPointListener;
import ch.hevs.isi.core.FloatDataPoint;
import ch.hevs.isi.utils.Utility;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.Timer;
import java.util.TimerTask;

/**
 * FieldConnector is the link between the program and Minecraft world
 */
public class FieldConnector implements DataPointListener {
    //Private attribute
    private static FieldConnector instance = null;

    //Registers
    BooleanRegister br;
    FloatRegister fr;

    /**
     * Creates all registers from the resource "ModbusEA_SIn.csv"
     */
    public void addRegister() {
        BufferedReader csv = Utility.fileParser(null, "ModbusEA_SIn.csv");  //Load

        if (csv != null) {
            try {
                String line = csv.readLine();                               //Reads one line
                String[] lineRead = line.split(";");                 //Separates the params

                while(line != null)
                {
                    if(lineRead[1].equals("[ON|OFF]"))                      //Creates registers according to their required type
                    {
                        new BooleanRegister(lineRead[0], Integer.parseInt(lineRead[5]), Integer.parseInt(lineRead[6]), Integer.parseInt(lineRead[7]));
                    }else {
                        new FloatRegister(lineRead[0], Integer.parseInt(lineRead[5]), Integer.parseInt(lineRead[6]), Integer.parseInt(lineRead[7]));
                    }

                    line = csv.readLine();                                  //Repeat operation

                    if (line != null) {
                        lineRead = line.split(";");
                    }
                }

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Constructor
     */
    private FieldConnector(){
    }

    /**
     * The static method getInstance () returns a reference to the singleton.
     * It creates the single FieldConnector object if it does not exist.
     * @return Returns the instance of FieldConnector
     */
    public static FieldConnector getInstance(){
        if(instance == null){
            instance = new FieldConnector();
        }
        return instance;
    }

    /**
     * Push to field label and value
     * @param label DataPoint's label (String)
     * @param value DataPoint's label (String)
     * @throws IOException
     */
    private void pushToField(String label, String value){
        DataPoint dp;

        try {
            if(value.equals("true")||value.equals("false"))                             //Boolean
            {
                dp = BinaryDataPoint.getDataPointFromLabel(label);                      //Retrieves the datapoint
                br = BooleanRegister.getRegisterFromDataPoint((BinaryDataPoint) dp);    //Retrieves the register
                br.write();
            }else{                                                                      //Float
                dp = FloatDataPoint.getDataPointFromLabel(label);                       //Retrieves the datapoint
                fr = FloatRegister.getRegisterFromDataPoint((FloatDataPoint) dp);       //Retrieves the register
                fr.write();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Updates a new float value
     * @param fdp FloatDataPoint (Object)
     * @throws IOException
     */
    @Override
    public void onNewValue(FloatDataPoint fdp) throws IOException {
        pushToField(fdp.getLabel(), Float.toString(fdp.getValue()));
    }

    /**
     * Updates a new float value
     * @param bdp FloatDataPoint (Object)
     * @throws IOException
     */
    @Override
    public void onNewValue(BinaryDataPoint bdp) throws IOException {
        pushToField(bdp.getLabel(), Boolean.toString(bdp.getValue()));
    }

    /**
     * Sets a polling routine every second
     */
    public void timedPolling(){
        Timer pollTimer = new Timer();

        pollTimer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                try {
                    BooleanRegister.poll();
                    FloatRegister.poll();
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        },0, 1000);
    }

    /**
     * Main function used to test field component
     * @param args
     */
    public static void main(String[] args) {
        FieldConnector fc = FieldConnector.getInstance();   //Creates connector

        fc.addRegister();                                   //Creates registers
        fc.timedPolling();                                  //Starts polling
    }
}
