package ch.hevs.isi.field;

import ch.hevs.isi.core.DataPoint;
import ch.hevs.isi.core.FloatDataPoint;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * Register that binds a FloatDatapoint to ModbusAccessor
 */
public class FloatRegister {
    //Modbus credentials
    static String SERVER_NAME = "localhost";
    static int SERVER_PORT = 1502;

    //Field variables
    int range;
    int offset;
    int address;
    FloatDataPoint dataPoint;

    ModbusAccessor ma = new ModbusAccessor(SERVER_PORT, SERVER_NAME);

    //Map containing all floatRegisters
    static Map<DataPoint, FloatRegister> fRegisterMap = new HashMap<>();

    /**
     * Constructor
     * @param label Label of the field
     * @param address Address of the the register
     * @param range Range of the value
     * @param offset Offset if it has one
     * @throws IOException
     */
    public FloatRegister(String label, int address,int range, int offset) throws IOException {
        this.address = address;
        this.range = range;
        this.offset = offset;

        dataPoint = (FloatDataPoint) FloatDataPoint.getDataPointFromLabel(label);   //Seek the corresponding datapoint
                                                                                    //in the map (core)
        if(dataPoint == null){                                                      //Datapoint not created yet
            if(address == 205 || address == 209) {                                  //It is an input
                dataPoint = new FloatDataPoint(label, true);
            }else{                                                                  //It is an output
                dataPoint = new FloatDataPoint(label, false);
            }
        }
        fRegisterMap.put(dataPoint, this);                                          //Stocks the datapoint in the map
    }

    /**
     * Reads the  measure through modbus and update value
     * @throws IOException
     */
    public void read() throws IOException {
        dataPoint.setValue(ma.readFloat(address)*range+offset);
    }

    /**
     * Writes a new value in minecraft through modbus
     * @throws IOException
     */
    public void write() throws IOException {
        ma.writeFloat(address, dataPoint.getValue());
    }

     /**
     * Retrieves a register stocked in the map from a datapoint
     * @param fdp FloatDataPoint
     * @return Returns the corresponding register
     */
    static FloatRegister getRegisterFromDataPoint(FloatDataPoint fdp)
    {
        return fRegisterMap.get(fdp);
    }

    /**
     * Read action for every member of the map. Global update of every register
     * @throws Exception
     */
    public static void poll() throws Exception {
        for (Map.Entry<DataPoint, FloatRegister> entry : fRegisterMap.entrySet()) {
            fRegisterMap.get(entry.getKey()).read();
        }
    }
}
