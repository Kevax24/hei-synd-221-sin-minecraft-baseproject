package ch.hevs.isi.field;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.nio.ByteBuffer;

/**
 * Access the modbus server in the minecraft world. Is the link between clients and minecraft.
 */
public class ModbusAccessor extends Socket {
    static String SERVER_NAME = "localhost";
    static int SERVER_PORT = 1502;

    OutputStream out;
    InputStream in;

    short readFloatId = 0x01 ;
    short readBooleanId = 0x02 ;
    short writeFloatId = 0x03 ;
    short writeBooleanId = 0x04 ;

    /**
     * Constructor
     * @param port the server's port number
     * @param name the server's name
     * @throws IOException
     */
    public ModbusAccessor(int port, String name) throws IOException {
        super(name, port);
        out = this.getOutputStream();
        in = this.getInputStream();
        ByteBuffer theBB = ByteBuffer.allocate(7);
    }

    /**
     * Creates the mbap header to transmit
     * @param BB Buffer in which the frame is set
     * @param transId ID of the modbus request
     * @param length Number of following bytes
     * @param unitId ID of remote slave. Set to 1 by default
     */
    void setMBAPHeader(ByteBuffer BB, short transId, short length, byte unitId){           // trans id -> 2 bytes
        BB.putShort(transId);                                                              // protocol id = 0 -> 2 bytes
        BB.putShort((short) 0);                                                            // length -> 2 bytes
        BB.putShort(length);                                                               // unit id -> 1 byte
        BB.put(unitId);
    }

    /**
     * Read Holding Registers (float)
     * @param regAddress Address of the register to access
     * @return  returns the value of the float
     * @throws IOException
     */
    float readFloat(int regAddress)throws IOException {
        ByteBuffer BB = ByteBuffer.allocate(12);                        //12 = 7header + 5 request
        setMBAPHeader(BB, readFloatId, (short) 5,(byte) 1);             // readFloatId = 1(free to choice) / 5 = number of bytes // 1 = slide pdf
        BB.put((byte)0x03);                                             //(0x03) Read Holding Registers
        BB.putShort((short)regAddress);
        BB.putShort((short)0x02);                                       // range & offset

        out.write(BB.array());                                          // send the modbus request
        byte[] rxPdu = new byte[13];                                    // 13 = 7 header +  6 responses
        in.read(rxPdu);                                                 // reception of the responses in rxPdu
        ByteBuffer rxPduBB = ByteBuffer.wrap(rxPdu);                    //transform rxpdu in byteBuffer

        //the value of the float is at the 9 position (7 header + 1 function code + 1 byte count)
        return rxPduBB.getFloat(9);
    }

    /**
     * Read Coils (boolean) on a given register
     * @param regAddress Address of the register to access
     * @return returns the value of the boolean
     * @throws Exception
     */
    boolean readBoolean(int regAddress) throws Exception {
        ByteBuffer BB = ByteBuffer.allocate(12);                            //12 = 7 header + 5 request
        setMBAPHeader(BB, readBooleanId, (short) 5, (byte) 1);              //readBooleanId = 2(free choice) / 5 = number of bytes // 1 = slide pdf
        BB.put((byte)0x01);                                                 //(0x01) Read Coils
        BB.putShort((short)regAddress);
        BB.putShort((short)0x01);                                           // no offset for boolean's value

        out.write(BB.array());                                              // send the modbus request
        byte[] rxPdu = new byte[10];                                        //10 = 7 header + 3 responses
        in.read(rxPdu);                                                     //reception of the responses in rxPdu
        ByteBuffer rxPduBB = ByteBuffer.wrap(rxPdu);                        //transform rxpdu in byteBuffer

        if((rxPduBB.get(9)&0x01)==0) {
            return false ;
        }
        else{
            return true ;
        }
    }

    /**
     * write a float value on a given register
     * @param regAddress Address of the register to access
     * @param newValue  the new value to write
     * @throws IOException
     */
    void writeFloat(int regAddress, float newValue) throws IOException {
        ByteBuffer BB = ByteBuffer.allocate(17);                                //7 header + 10 request
        setMBAPHeader(BB, writeFloatId, (short) 10, (byte) 1);
        BB.put((byte) 0x10);                                                     // (0x10) Write Multiple registers
        BB.putShort((short) regAddress);
        BB.putShort((short) 2);
        BB.put((byte) 0x04);                                                     // 2*2 registers
        BB.putFloat(newValue);

        out.write(BB.array());
        byte[] rxPdu = new byte[12];                                            // 12 = 7 header + 5 responses
        in.read(rxPdu);                                                         // here we need to see if the reponse is good
    }                                                                           // and also to wait the response for the modbus
                                                                                // function (replace the timer)

    /**
     * write a boolean value on a given register
     * @param regAddress Address of the register to access
     * @param newValue  the new value to write
     * @throws IOException
     */
    void writeBoolean(int regAddress, boolean newValue) throws IOException {
        ByteBuffer BB = ByteBuffer.allocate(12);                                //7 header + 5 request
        setMBAPHeader(BB, writeBooleanId, (short) 5, (byte) 1);
        BB.put((byte)0x05);                                                     //(0x05) Write Single Coil
        BB.putShort((short)regAddress);
        BB.putShort((short)(newValue?0xff00:0x00));                             // 0XFF00 requests the coil to be ON. A value of 0X0000 requests the coil to be off.

        out.write(BB.array());
        byte[] rxPdu = new byte[12];                                            // 12 = 7 header + 5 responses
        in.read(rxPdu);
    }

    /**
     * Main function used to test ModbusAccessor
     * @param args
     * @throws Exception
     */
    public static void main(String[] args) throws Exception {
        ModbusAccessor mb = new ModbusAccessor(SERVER_PORT, SERVER_NAME);

        System.out.println(mb.readBoolean(405));        //Get remote wind status
        mb.writeBoolean(405, false);          //Turn off wind
        mb.writeBoolean(405, true);           //Turn on wind
        System.out.println(mb.readFloat(205));          //Get factory setpoint
        mb.writeFloat(205, 0.5f);             //Set factory setpoint to 60%
        System.out.println(mb.readFloat(205));          //Get factory setpoint
    }
}
