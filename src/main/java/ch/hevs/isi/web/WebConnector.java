package ch.hevs.isi.web;

import ch.hevs.isi.core.BinaryDataPoint;
import ch.hevs.isi.core.DataPoint;
import ch.hevs.isi.core.DataPointListener;
import ch.hevs.isi.core.FloatDataPoint;
import ch.hevs.isi.field.FieldConnector;
import org.java_websocket.WebSocket;
import org.java_websocket.handshake.ClientHandshake;
import org.java_websocket.server.WebSocketServer;
import java.io.IOException;
import java.net.InetSocketAddress;

/**
 * WebConnector generates web socket server accepting parallel connections from
 * multiple web browser pages on port 8888.
 */
public class WebConnector extends WebSocketServer implements DataPointListener {

    private static final int SERVER_PORT = 8888;
    private static WebConnector instance = null;

    /**
     * Creates a web socket server
     */
    private WebConnector(){
        super(new InetSocketAddress(SERVER_PORT));  //Implements an IP Socket Address (IP address + port number)

        this.start();
    }

    /**
     * Called after an opening handshake has been performed and the given websocket is ready to be written on.
     * @param webSocket The WebSocket instance this event is occuring on.
     * @param clientHandshake The handshake of the websocket instance
     */
    @Override
    public void onOpen(WebSocket webSocket, ClientHandshake clientHandshake) {
        this.addConnection(webSocket);          //Connect server with WebSocket
        webSocket.send("Welcome");
        System.out.println("Welcome to server");
    }

    /**
     * Called after the websocket connection has been closed.
     * @param webSocket The WebSocket instance this event is occuring on.
     * @param i The codes can be looked up here: CloseFrame
     * @param s Additional information string
     * @param b Returns whether or not the closing of the connection was initiated by the remote host.
     */
    @Override
    public void onClose(WebSocket webSocket, int i, String s, boolean b) {
        this.removeConnection(webSocket);
        System.out.println("Exit page");
    }

    /**
     * Callback for string messages received from the remote host
     * @param webSocket The WebSocket instance this event is occuring on.
     * @param s The UTF-8 decoded message that was received.
     */
    @Override
    public void onMessage(WebSocket webSocket, String s) {
        String[] message = s.split("=");

        try {
            if(DataPoint.getDataPointFromLabel(message[0]) == null){
            }
            else if (message[1].equals("true") || message[1].equals("false")){
                BinaryDataPoint.getDataPointFromLabel(message[0]).setValue(Boolean.parseBoolean(message[1]));   //Update new value
            } else{
                FloatDataPoint.getDataPointFromLabel(message[0]).setValue(Float.parseFloat(message[1]));        //Update new value
            }

            pushToWebPages(message[0], message[1]);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Called when errors occurs. If an error causes the websocket connection to fail
     * onClose(WebSocket, int, String, boolean) will be called additionally.
     * This method will be called primarily because of IO or protocol errors.
     * If the given exception is an RuntimeException that probably means that you encountered a bug.
     * @param webSocket Can be null if there error does not belong to one specific websocket. For example if the servers port could not be bound.
     * @param e The exception causing this error
     */
    @Override
    public void onError(WebSocket webSocket, Exception e) {
    }

    /**
     * Called when the server started up successfully. If any error occured, onError is called instead.
     */
    @Override
    public void onStart() {
    }

    /**
     * Creates the single WebConnector object if it does not exist.
     * @return a reference to the singleton
     */
    public static WebConnector getInstance(){
        if(instance == null){
            instance = new WebConnector();
        }
        return instance;
    }

    /**
     * Push to all web pages label and value
     * @param label DataPoint's label (String)
     * @param value DataPoint's value (String)
     */
    private void pushToWebPages(String label, String value) throws IOException{
        this.broadcast(label + "=" + value);

        System.out.println("Web : Label = " + label + ", Value = " + value);
    }

    /**
     * Updates a new float value
     * @param fdp FloatDataPoint (Object)
     */
    @Override
    public void onNewValue(FloatDataPoint fdp) {
        try {
            pushToWebPages(fdp.getLabel(), Float.toString(fdp.getValue()));
        }catch(IOException e){
            e.printStackTrace();
            System.out.println("Error on new value FloatDataPoint in WebConnector");
        }
    }

    /**
     * Updates a new binary value
     * @param bdp BinaryDataPoint (Object)
     */
    @Override
    public void onNewValue(BinaryDataPoint bdp) {
        try {
            pushToWebPages(bdp.getLabel(), Boolean.toString(bdp.getValue()));
        }catch(IOException e){
            e.printStackTrace();
            System.out.println("Error on new value BinaryDataPoint in WebConnector");
        }
    }

    /**
     * Main function used to test web component
     * @param args
     */
    public static void main(String[] args) {
        FieldConnector.getInstance().addRegister();
        WebConnector wc = WebConnector.getInstance();
    }
}
